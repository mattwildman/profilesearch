var urlList = new Array();
var setUrlList = false;

function loadContensisCentralColumn(url) {
	var resDiv = document.createElement('div');
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, false);
	xhr.send(null);
	resDiv.innerHTML = xhr.response;
	var finResponse = resDiv.children[resDiv.childElementCount - 1]
							.children[13]
							.children[1]
							.children[1]
							.children[0]
							.children[0]
							.children[0]
							.children[0]
							.children[1];
	return finResponse; //returns a td element
}

function loadContensisCentralColumn2(url) {
	var resDiv = document.createElement('div');
	var xmlhttp;
	var finResponse;
	if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
		}
	else
		{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			resDiv.innerHTML = xmlhttp.response;
			finResponse = resDiv.children[resDiv.childElementCount - 1]
									.children[13]
									.children[1]
									.children[1]
									.children[0]
									.children[0]
									.children[0]
									.children[0]
									.children[1];
		}
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
	return finResponse;
}

function createUrlList() {
	if (!setUrlList) {
		var urlElem = loadContensisCentralColumn2('/wildmanm/profilesearch/sitemap.html').children[2];
		urlList = urlElem.getElementsByTagName('a');
		setUrlList = true;
	}
	return urlList;
}

function matchesDept(response) {
	var searchDept = document.getElementById('department').value;
	if (searchDept == 'all')
		return true;
	var dept = response.children[2].children[0].children[0].getAttribute('class');
	return dept.indexOf(searchDept) != -1;
}

function matchesIndustry(response) {
	var searchInd = document.getElementById('industry').value;
	if (searchInd == 'all')
		return true;
	var ind = response.children[2].children[0].children[1].getAttribute('class');
	return ind.indexOf(searchInd) != -1;
}

function matchesSearch(response) {
	return matchesDept(response) && matchesIndustry(response);
}

function prepareResult(response, a) {
	
	//create final search result div
	var finDiv = document.createElement('div');
	var nameTxt = response.children[0].firstChild;
	var name = document.createElement('h3');
	name.appendChild(nameTxt);
	var details = response.children[2].children[0].children[2];
	finDiv.appendChild(name);
	finDiv.appendChild(details);
	
	//create and append link to full profile
	var link = document.createElement('a');
	link.setAttribute('href', a);
	link.setAttribute('target', '_blank');
	link.innerHTML = "View full profile >>";
	var linkP = document.createElement('p');
	linkP.appendChild(link);
	finDiv.appendChild(linkP);
	
	finDiv.setAttribute('class', 'search-result');
	finDiv.setAttribute('style', 'border-bottom: dotted 1px #7F7F7F;');
	return finDiv;
}

function displayProfiles(urlList) {
	for (var i = 0; i < urlList.length; i++) {
		var candidate = loadContensisCentralColumn2(urlList[i]);
		if (matchesSearch(candidate)) {
			var resDiv = prepareResult(candidate, urlList[i]);
			document.getElementById('results').appendChild(resDiv);
		}
	}
}

function searchProfiles() {
	document.getElementById('results').innerHTML = "<img id='loader' src='/wildmanm/profilesearch/loader.gif' align='center' />";
	displayProfiles(createUrlList());
	document.getElementById('results').removeChild(document.getElementById('loader'));
	if (document.getElementById('results').childElementCount < 1) {
		document.getElementById('results').innerHTML = "<p>There are no graduate profiles matching your search criteria.</p>";
	}
}