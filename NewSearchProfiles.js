function loadXML(url) {
	// code for Mozilla, Firefox, Opera, etc.
	if (window.XMLHttpRequest) {
	  var xhttp = new XMLHttpRequest();
	}
	// code for IE
	else {
	  var xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xhttp.open("GET",url,false);
	xhttp.send();
	return xhttp.responseXML;
}

function transformXML(xml, xsl) {
	// code for IE
	if (window.ActiveXObject) {
	  var ex = xml.transformNode(xsl);
	  document.getElementById("results").innerHTML = ex;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else if (document.implementation && document.implementation.createDocument) {
	  var xsltProcessor = new XSLTProcessor();
	  xsltProcessor.importStylesheet(xsl);
	  var resultDocument = xsltProcessor.transformToFragment(xml,document);
	  return resultDocument;
	}
}

function searchProfiles(dept, ind) {
	document.getElementById('query').innerHTML = "<p>Results for <strong id='dept-query'>" + dept + 
													"</strong> graduates working in <strong id='ind-query'>" + ind +
													"</strong> careers...</p>";
	document.getElementById('results').innerHTML = "<img id='loader' src='/wildmanm/profilesearch/loader.gif' />";
	var directoryURL = 'profileDirectory.xml';
	var directoryStyleURL = 'profileResults.xsl';
	directoryXML = loadXML(directoryURL);
	directoryXSL = loadXML(directoryStyleURL);
	var baseFilter = 'directory/profile'; //probably should take this directly from xsl doc
	var searchFilter = dept == 'all' ? 	'' : 
										'[education/department=\'' + dept + '\']';
	searchFilter = ind == 'all' ? searchFilter + '' : 
								  searchFilter + '[occupation/industry=\'' + ind + '\']';
	searchFilter = baseFilter + searchFilter;
	if (searchFilter != baseFilter) {
		if(directoryXSL.evaluate) { //moz etc
			var nsResolver = directoryXSL.createNSResolver (directoryXSL.ownerdocument == null ?
															directoryXSL.documentElement :
															directoryXSL.ownerdocument.documentElement);
			var resultNode = directoryXSL.evaluate('//xsl:for-each', directoryXSL, nsResolver, 
										XPathResult.ANY_UNORDERED_NODE_TYPE, null);
			resultNode.singleNodeValue.setAttribute('select', searchFilter);
		}
		else { //ie
			var resultNode = directoryXSL.selectSingleNode('//xsl:for-each');
			resultNode.setAttribute('select', searchFilter);
		}
	}
	document.getElementById('results').removeChild(document.getElementById('loader'));
	var resultDocument = transformXML(directoryXML, directoryXSL);
	document.getElementById('results').appendChild(resultDocument);
	if (document.getElementById('results').childElementCount < 1) {
		document.getElementById('results').innerHTML = "<p>There are no graduate profiles matching your search criteria.</p>";
	}
	var hscroll = (document.all ? document.scrollLeft : window.pageXOffset);
	window.scrollTo(hscroll, 0);
}