<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:for-each select="directory/profile">
			<xsl:sort select="education/year" order="descending" />
			
			<div style="border-bottom: dotted 1px #7F7F7F; overflow: auto; padding-bottom: 5px;">
				
				<img style="float:right; margin-top: 20px; margin-left: 10px;" height="100" src="{picture}" />
				
				<h3><xsl:value-of select="name" /></h3>
				
				<p>
				<xsl:for-each select="education">
					<strong>(<xsl:value-of select="degree" />) <xsl:value-of select="course" />, <xsl:value-of select="year" /></strong><br />
				</xsl:for-each>
				</p>
				
				<p><strong><xsl:value-of select="occupation/jobtitle" />, <xsl:value-of select="occupation/organisation" /></strong></p>
				
				<p><a href="{link}">View the full profile...</a></p>
				
				<ul class="industry-list">
					<xsl:for-each select="occupation/industry">
						<xsl:sort select="." />
						<li onclick="searchProfiles('all', $(this).text());"><xsl:value-of select="." /></li>
					</xsl:for-each>
				</ul>
				
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>